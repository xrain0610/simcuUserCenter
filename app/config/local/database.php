<?php

return array(
	'connections' => array(

		'mysql' => array(
			'driver'    => 'mysql',
			'host'      => 'localhost',
			'database'  => 'usercenter',
			'username'  => 'root',
			'password'  => '',
		),
	),
);
