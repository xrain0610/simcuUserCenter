<?php

return array(
	'driver' => 'eloquent',
	'model' => 'Login',
	'table' => 'Login',
	'reminder' => array(
		'email' => 'emails.auth.reminder',
		'table' => 'password_reminders',
		'expire' => 1200,
	),

);
