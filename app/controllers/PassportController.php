<?php
class PassportController extends BaseController {
	protected $layout = 'layouts.passport';

	public function getRegister(){
		if(Auth::check() || Auth::viaRemember()){
			$this->layout->content = View::make('passport.notice',['notice' => trans('passport.loginnotregister'),'backto'=>action('HomeController@getIndex')]);
		}else{
			$this->layout->content = View::make('passport.register');
		}
	}

	public function postRegister(){
		$pin = [
			'username' => Input::get('username'),
			'password' => Input::get('password'),
			'password_confirmation' => Input::get('confpass'),
			'agreed' => Input::get('agreed'),
		];
		$vali_pin = [
			'username' => 'required|regex:/^[a-zA-Z]+[a-zA-Z0-9_]*$/|between:5,20|unique:logins',
			'password' => 'required|min:8|confirmed',
			'agreed' => 'accepted',
		];
		$valires = Validator::make($pin,$vali_pin);
		if ($valires->fails()){	
			$errmsg = $valires->messages();
			$msg = "";
			foreach ($errmsg->all('<li>:message</li>') as $message){
				$msg .= $message;
			}
			$this->layout->content = View::make('passport.notice',['notice' => $msg,'backto'=>action('PassportController@getRegister')]);
		}else{
			$ulogin = new Login;
			$uinfo = new Userinfo;
			$ulogin->username = $pin['username'];
			$ulogin->password = Hash::make($pin['password']);
			if($ulogin->save()){
				$this->layout->content = View::make('passport.notice',['notice' => trans('passport.successregister') ,'backto'=>action('PassportController@getLogin')]);
			}
		}
	}

	public function getLogin(){
		if(Auth::check() || Auth::viaRemember()){
			return Redirect::to(Session::pull('logincb', action('HomeController@getIndex')));
		}else{
			$this->layout->content = View::make('passport.login');
		}
	}

	public function postLogin(){
		$pin = [
			'username' => Input::get('username'),
			'password' => Input::get('password'),
		];
		$vali_pin = [
			'username' => 'required|login',
			'password' => 'required|min:8',
		];
		$valires = Validator::make($pin,$vali_pin);
		if ($valires->fails()){	
			$errmsg = $valires->messages();
			$msg = "";
			foreach ($errmsg->all('<li>:message</li>') as $message){
				$msg .= $message;
			}
			$this->layout->content = View::make('passport.notice',['notice' => $msg,'backto'=>action('PassportController@getLogin')]);
		}else{
			if(Custools::checkEmail($pin['username'])){
				$authres = Auth::attempt(['email'=>$pin['username'],'email_verify' => 1,'password'=>$pin['password'],'status'=>'ACTIVE'],Input::get('keeplogin'));
			}elseif(Custools::checkCell($pin['username'])){
				$authres = Auth::attempt(['cell'=>$pin['username'],'cell_verify' => 1,'password'=>$pin['password'],'status'=>'ACTIVE'],Input::get('keeplogin'));
			}else{
				$authres = Auth::attempt(['username'=>$pin['username'],'password'=>$pin['password'],'status'=>'ACTIVE'],Input::get('keeplogin'));
			}
			if ($authres){
			   	$this->layout->content = View::make('passport.notice',['notice' => trans('passport.successlogin'),'backto'=>Session::pull('logincb', action('HomeController@getIndex'))]);
			}else{
				$this->layout->content = View::make('passport.notice',['notice' => trans('passport.wronglogin'),'backto'=>action('PassportController@getLogin')]);
			}
		}
	}

	public function getLogout(){
		Auth::logout();
		$this->layout->content = View::make('passport.notice',['notice' => trans('passport.logout'),'backto'=>action('PassportController@getLogin')]);
	}

	public function getEmailverify(){
		$exp = 48 * 3600;
		if(Input::get('key')){
			$vinfo = Verifyemail::where('verify_key','=',Input::get('key'))->first();
			if($vinfo){
				if($vinfo->status == 1){
					$msg = '<p>'.trans('passport.verifysuccess').'</p>';
					$this->layout->content = View::make('passport.notice',['notice' => $msg,'backto'=>action('PassportController@getLogin')]);
				}
				$uid = $vinfo->uid;
				$mail = $vinfo->email;
				$vinfo->status = 1;
				$vinfo->save();
				$login = Login::find($uid);
				if($mail == $login->email and strtotime("+48 hours",$vinfo->create_at > time()) and $login->email_verify == 0){
					$login->email_verify = 1;
					$login->save();
					$msg = '<p>'.trans('passport.verifysuccess').'</p>';
					$this->layout->content = View::make('passport.notice',['notice' => $msg,'backto'=>action('PassportController@getLogin')]);
				}else{
					$msg = '<p>'.trans('passport.verifykeyouttime').'</p>';
					$this->layout->content = View::make('passport.notice',['notice' => $msg,'backto'=>action('PassportController@getLogin')]);
				}
			}else{
				$msg = '<p>'.trans('passport.verifykeyouttime').'</p>';
				$this->layout->content = View::make('passport.notice',['notice' => $msg,'backto'=>action('PassportController@getLogin')]);
			}
		}else{
			App::abort(404);
		}
	}
	
	public function getLocksys(){
		Session::put('syslock',true);
		$uinfo = [
			'photo' => 1,
			'showname' => 1,
		];
		$this->layout->content = View::make('passport.lock',$uinfo);
	}
}