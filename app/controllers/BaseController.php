<?php

class BaseController extends Controller {
	protected function setupLayout(){
		if ( ! is_null($this->layout)){
			$layoutname = $this->layout;
			$this->layout = View::make($this->layout);

			if($layoutname == 'layouts.home'){
				if(Auth::user()->userinfo->name){
					$this->layout->showname = Auth::user()->userinfo->name;
				}else{
					$this->layout->showname = Auth::user()->username;
				}
				$this->layout->username = Auth::user()->username;
				$this->layout->userid = Auth::id();
				$this->layout->uphoto = Auth::user()->userinfo->photo;
			}

		}
	}
}