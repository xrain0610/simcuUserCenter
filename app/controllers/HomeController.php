<?php

class HomeController extends BaseController {
	protected $layout = 'layouts.home';
	public function getIndex(){
		$this->layout->content = View::make('home.index');
	}

	public function getProfile(){
		$ret = [
			'showname' => Auth::user()->userinfo->name,
			'sex' => Auth::user()->userinfo->sex,
			'signture' => Auth::user()->userinfo->signture,
		];
		$this->layout->content = View::make('home.profile',Auth::user()->userinfo);
	}

	public function postProfile(){
		$uinfo = Userinfo::find(Auth::id());
		if (Input::hasFile('photo')){
		    $uphoto = Input::file('photo');
		    Input::file('photo')->move(Config::get('app.photo_path'), Auth::id()."-".time().".".Input::file('photo')->getClientOriginalExtension());
		    $uinfo->photo = "/".Config::get('app.photo_path').Auth::id()."-".time().".".Input::file('photo')->getClientOriginalExtension();
			$uinfo->save();
			return Redirect::action('HomeController@getProfile');
		}else{
			$uinfo->name = Input::get('name')?Input::get('name'):Auth::user()->userinfo->name;
			$uinfo->signture = Input::get('signture')?Input::get('signture'):Auth::user()->userinfo->signture;
			$uinfo->email = Input::get('email')?Input::get('email'):Auth::user()->userinfo->email;
			$uinfo->sex = Input::get('sex')?Input::get('sex'):Auth::user()->userinfo->sex;
			$uinfo->birthday = Input::get('birthday')?Input::get('birthday'):Auth::user()->userinfo->birthday;
			$uinfo->country = Input::get('country')?Input::get('country'):Auth::user()->userinfo->country;
			$uinfo->province = Input::get('province')?Input::get('province'):Auth::user()->userinfo->province;
			$uinfo->city = Input::get('city')?Input::get('city'):Auth::user()->userinfo->city;
			$uinfo->address = Input::get('address')?Input::get('address'):Auth::user()->userinfo->address;
			$uinfo->realname = Input::get('realname')?Input::get('realname'):Auth::user()->userinfo->realname;
			$uinfo->idcard = Input::get('idcard')?Input::get('idcard'):Auth::user()->userinfo->idcard;
			$uinfo->language = Input::get('language')?Input::get('language'):Auth::user()->userinfo->language;
			$uinfo->vocation = Input::get('vocation')?Input::get('vocation'):Auth::user()->userinfo->vocation;
			$uinfo->school = Input::get('school')?Input::get('school'):Auth::user()->userinfo->school;
			$uinfo->company = Input::get('company')?Input::get('company'):Auth::user()->userinfo->company;
			$uinfo->personal_web = Input::get('personal_web')?Input::get('personal_web'):Auth::user()->userinfo->personal_web;
			$uinfo->save();
			return '1';	
		}
	}

	public function getSafe(){
		$this->layout->content = View::make('home.safe',Auth::user());
	}

	public function postSafeqa(){
		if(Auth::user()->userinfo->safeq1){
			if(Input::get('safea1') == Auth::user()->userinfo->safea1 
			&& Input::get('safea2') == Auth::user()->userinfo->safea2 
			&& Input::get('safea3') == Auth::user()->userinfo->safea3){
				return Response::json(['status' => 1]);
			}else{
				return Response::json(['status' => 0]);
			}
		}else{
			$pin = [
				'safeq1' => Input::get('safeq1'),
				'safeq2' => Input::get('safeq2'),
				'safeq3' => Input::get('safeq3'),
				'safea1' => Input::get('safea1'),
				'safea2' => Input::get('safea2'),
				'safea3' => Input::get('safea3'),
				];
			$vali_pin = [
				'safeq1' => 'required|between:3,20|different:safeq2',
				'safeq2' => 'required|between:3,20|different:safeq3',
				'safeq3' => 'required|between:3,20|different:safeq1',
				'safea1' => 'required|between:3,20',
				'safea2' => 'required|between:3,20',
				'safea3' => 'required|between:3,20',
			];
			$valires = Validator::make($pin,$vali_pin);
			if ($valires->fails()){	
				$errmsg = $valires->messages();
				$msg = "";
				foreach ($errmsg->all('<li>:message</li>') as $message){
					$msg .= $message;
				}
				$this->layout->content = View::make('passport.notice',['notice' => $msg,'backto'=>action('HomeController@getSafe')]);
			}else{
				$uinfo = Userinfo::find(Auth::id());
				$uinfo->safeq1 = Input::get('safeq1');
				$uinfo->safeq2 = Input::get('safeq2');
				$uinfo->safeq3 = Input::get('safeq3');
				$uinfo->safea1 = Input::get('safea1');
				$uinfo->safea2 = Input::get('safea2');
				$uinfo->safea3 = Input::get('safea3');
				$uinfo->save();
				return Redirect::action('HomeController@getSafe');
			}
		}
	}

	public function postSafeemail(){
		$pin = [
			'email' => Input::get('email'),
			];
		$vali_pin = [
			'email' => 'required|email|unique:logins,email,'.Auth::id(),
			];
		$valires = Validator::make($pin,$vali_pin);
		if ($valires->fails()){
			$errmsg = $valires->messages();
			$msg = "";
			foreach ($errmsg->all('<li>:message</li>') as $message){
				$msg .= $message;
			}
			$this->layout->content = View::make('passport.notice',['notice' => $msg,'backto'=>action('HomeController@getSafe')]);
		}else{
			$ulogin = Login::find(Auth::id());
			$ulogin->email = Input::get('email');
			$ulogin->cell_verify = 0;
			$ulogin->save();
			$vk = new Verifyemail;
			$vk->uid = Auth::id();
			$vk->email = Input::get('email');
			$vk->verify_key = Crypt::encrypt(Auth::id().rand(100000000000,999999999999));
			$vk->save();
			//此处发送邮件
			$msg = "<p>".trans('home.mailsended')."</p>";
			$this->layout->content = View::make('passport.notice',['notice' => $msg,'backto'=>action('HomeController@getSafe')]);
		}
	}

	public function postSafecell(){
		$pin = [
			'cell' => Input::get('cell'),
			'smscode' => Input::get('smscode'),
			'smscodesys' => Session::get('smscode'),
			];
		$vali_pin = [
			'cell' => 'required|regex:/^1[3,4,5,7,8][0-9]\d{4,8}$/|unique:logins,cell',
			'smscode' => 'required|digits:6|same:smscodesys',
			];
		$valires = Validator::make($pin,$vali_pin);
		if ($valires->fails()){
			$errmsg = $valires->messages();
			$msg = "";
			foreach ($errmsg->all('<li>:message</li>') as $message){
				$msg .= $message;
			}
			$this->layout->content = View::make('passport.notice',['notice' => $msg,'backto'=>action('HomeController@getSafe')]);
		}else{
			if(Auth::user()->cell){
				abort(404);
			}else{
				$ulogin = Login::find(Auth::id());
				$ulogin->cell = Input::get('cell');
				$ulogin->cell_verify = 1;
				$ulogin->save();
				return Redirect::action('HomeController@getSafe');
			}
		}
	}

	public function postSendsms(){
		$pin = [
			'cell' => Input::get('cell'),
			];
		$vali_pin = [
			'cell' => 'required|regex:/^1[3,4,5,7,8][0-9]\d{4,8}$/|unique:logins,cell',
			];
		$valires = Validator::make($pin,$vali_pin);
		if ($valires->fails()){
			$errmsg = $valires->messages();
			$msg = "";
			foreach ($errmsg->all(':message') as $message){
				$msg .= $message;
			}
			return Response::json(['status' => 0, 'msg' => $msg]);
		}else{
			if(!Session::get('smscodetime')){
				Session::put('smscodetime',time());
			}
			$smstimeleft = 60 - (time() - Session::get('smscodetime'));
			if($smstimeleft <= 0){
				Session::put('smscodetime',time());
				Session::put('smscode',rand(100000,999999));
				//此处发送短信
				return Response::json(['status' => 1, 'msg' => trans('home.smssended')."(我是验证码".Session::get('smscode').")"]);
			}else{
				return Response::json(['status' => 0, 'msg' => $smstimeleft.trans('home.smscooldown')]);
			}
		}
	}

	public function postSafeident(){
		$uinfo = Userinfo::find(Auth::id());
		if($uinfo->realname){
			App::abort(404);
		}else{
			$pin = [
				'realname' => Input::get('realname'),
				'idcard' => Input::get('idcard'),
				];
			$vali_pin = [
				'realname' => 'required|between:2,5',
				'idcard' => 'required|size:18',
				];
			$valires = Validator::make($pin,$vali_pin);
			if ($valires->fails()){	
				$errmsg = $valires->messages();
				$msg = "";
				foreach ($errmsg->all('<li>:message</li>') as $message){
					$msg .= $message;
				}
				$this->layout->content = View::make('passport.notice',['notice' => $msg,'backto'=>action('HomeController@getSafe')]);
			}else{
				$uinfo->idcard = Input::get('idcard');
				$uinfo->realname = Input::get('realname');
				$uinfo->save();
				return Redirect::action('HomeController@getSafe');
			}
		}
	}
}
