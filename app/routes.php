<?php
App::missing(function($exception){return Response::view('404', array(), 404);});
Route::get('passport/locksys', ['before'=>'auth','uses'=>'PassportController@getLocksys']);
Route::controller('passport', 'PassportController');
Route::controller('password', 'RemindersController');
Route::group(['before' => 'auth|lock'], function()
{
	Route::get('/', 'HomeController@getIndex');
    Route::controller('home', 'HomeController');
});