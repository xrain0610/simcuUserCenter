<?php

App::before(function($request)
{
	//
});


App::after(function($request, $response)
{
	//
});

Route::filter('auth', function()
{
	if (!Auth::check())
	{
		Session::put('logincb',Request::url());
		return Redirect::to(action('PassportController@getLogin'));	
	}
});

Route::filter('lock', function()
{
	if (Session::get('syslock'))
	{
		Session::put('lockfrom',Request::url());
		return Redirect::to(action('PassportController@getLocksys'));	
	}
});

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

Route::filter('csrf', function()
{
	if (Session::token() !== Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});
