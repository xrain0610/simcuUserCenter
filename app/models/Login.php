<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;

class Login extends Eloquent implements UserInterface{

	use UserTrait;
	protected $hidden = array('password', 'remember_token');

	public static function boot(){
        parent::boot();
        Login::created(function($login)
		{
		    $uinfo = new Userinfo;
		    $uinfo->id = $login->id;
		    $uinfo->save();
		});
    }

	public function userinfo(){
		return $this->hasOne('Userinfo','id','id');
	}
}
