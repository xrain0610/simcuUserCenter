<?php
class Custools{
	public static function checkEmail($str){
		if(preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/",$str)){
		    	return true;
		}
		return false;
	}

	public static function checkCell($str){
		if(preg_match("/^1[3|4|5|7|8][0-9]\d{4,8}$/",$str)){
		    	return true;
		}
		return false;
	}

	public static function checkLoginname($str){
		if(preg_match("/^[a-zA-Z]+[a-zA-Z0-9_]*$/",$str)){
		    	return true;
		}
		return false;
	}
}