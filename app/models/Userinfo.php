<?php

class Userinfo extends Eloquent{

	protected $touches = array('login');

	public function login(){
        return $this->belongsTo('Login','id','id');
    }
}
