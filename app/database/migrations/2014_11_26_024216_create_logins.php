<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogins extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('logins',function($table){
			$table->bigIncrements('id');
			$table->string('username',20)->nullable()->unique();
			$table->string('cell',11)->nullable()->unique();
			$table->boolean('cell_verify')->default(false);
			$table->string('email')->nullable()->unique();
			$table->boolean('email_verify')->default(false);
			$table->string('password');
			$table->enum('status',['ACTIVE','UNABLE','LOCK'])->default('ACTIVE');
			$table->timestamps();
			$table->rememberToken();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('logins');
	}

}
