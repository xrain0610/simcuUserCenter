<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserinfos extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('userinfos',function($table){
			$table->bigInteger('id')->unsigned();
			$table->primary('id');
			$table->string('name',8)->nullable();
			$table->string('photo')->default('/assets/img/nophoto.jpg');
			$table->string('signture',200)->nullable();
			$table->string('email')->nullable();
			$table->enum('sex',['GENDER','FEMALE'])->nullable();
			$table->string('birthday',10)->nullable();
			$table->string('country',30)->nullable();
			$table->string('province',30)->nullable();
			$table->string('city',30)->nullable();
			$table->string('address')->nullable();
			$table->string('realname',20)->nullable();
			$table->string('idcard',30)->nullable()->unique();
			$table->string('language',20)->nullable();
			$table->string('company',50)->nullable();
			$table->string('vocation',30)->nullable();
			$table->string('school',100)->nullable();
			$table->string('personal_web')->nullable();
			$table->string('safeq1')->nullable();
			$table->string('safea1')->nullable();
			$table->string('safeq2')->nullable();
			$table->string('safea2')->nullable();
			$table->string('safeq3')->nullable();
			$table->string('safea3')->nullable();
			$table->timestamps();
			$table->foreign('id')->references('id')->on('logins')->onUpdate('Cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('userinfos');
	}

}
