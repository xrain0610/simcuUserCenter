<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();
		$this->call('LoginTableSeeder');
	}
}

class LoginTableSeeder extends Seeder {

    public function run()
    {
        Login::create(['id'=>'100001','username'=>'xrain','password'=>'$2y$10$FaugQSnvb3SR6WFtjGD7LegaehfyZHnG8XLtEqheYEaItCcTnJHxO']);
    }

}
