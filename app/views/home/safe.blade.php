<div class="page-body">
	<div class="row">
		<div class="col-lg-12 col-sm-12 col-xs-12">
            <div class="widget">
                <div class="widget-header bg-blue">
	                <span class="widget-caption">{{trans('home.editprofile')}}</span>
	            </div>
                <div class="widget-body">
                <div class="tabbable">
                        <ul class="nav nav-tabs tabs-flat" id="myTab11">
                            <li class="active">
                                <a data-toggle="tab" href="#safeqa">
                                    {{trans('home.safeqa')}}
                                </a>
                            </li>
                            <li class="">
                                <a data-toggle="tab" href="#safeemail">
                                    {{trans('home.safeemail')}}
                                </a>
                            </li>
                            <li class="">
                                <a data-toggle="tab" href="#safecell">
                                    {{trans('home.safecell')}}
                                </a>
                            </li>
                            <li class="">
                                <a data-toggle="tab" href="#safeidentity">
                                    {{trans('home.safeidentity')}}
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content tabs-flat">
                            <div id="safeqa" class="tab-pane active">
                                <div id="form">
				                    <form role="form" id="safeqaform" method="post" action="{{action('HomeController@postSafeqa')}}">
			                            <div class="row">
			                                <div class="col-sm-6">
			                                    <div class="form-group">
			                                        <label>{{trans('home.safeq1')}}:</label>
			                                        @if(empty($userinfo['safeq1']))
			                                        	<input type="text" name="safeq1" class="form-control">
			                                        @else
			                                        	<p>{{$userinfo['safeq1']}}</p>
			                                        @endif
			                                    </div>
			                                </div>
			                                <div class="col-sm-6">
			                                    <div class="form-group">
			                                        <label>{{trans('home.safea1')}}</label>
			                                        <input type="text" id="safea1" name="safea1" class="form-control">
			                                    </div>
			                                </div>
			                            </div>
			                             <div class="row">
			                                <div class="col-sm-6">
			                                    <div class="form-group">
			                                        <label>{{trans('home.safeq2')}}:</label>
			                                        @if(empty($userinfo['safeq2']))
			                                        	<input type="text" name="safeq2" class="form-control">
			                                        @else
			                                        	<p>{{$userinfo['safeq2']}}</p>
			                                        @endif
			                                    </div>
			                                </div>
			                                <div class="col-sm-6">
			                                    <div class="form-group">
			                                        <label>{{trans('home.safea2')}}</label>
			                                        <input type="text" id="safea2" name="safea2" class="form-control">
			                                    </div>
			                                </div>
			                            </div>
			                             <div class="row">
			                                <div class="col-sm-6">
			                                    <div class="form-group">
			                                        <label>{{trans('home.safeq3')}}:</label>
			                                        @if(empty($userinfo['safeq3']))
			                                        	<input type="text" name="safeq3" class="form-control">
			                                        @else
			                                        	<p>{{$userinfo['safeq3']}}</p>
			                                        @endif
			                                    </div>
			                                </div>
			                                <div class="col-sm-6">
			                                    <div class="form-group">
			                                        <label>{{trans('home.safea3')}}</label>
			                                        <input type="text" id="safea3" name="safea3" class="form-control">
			                                    </div>
			                                </div>
			                            </div>
			                            @if(empty($userinfo['safeq1']))
			                            	<button type="submit" class="btn btn-blue">{{trans('home.setsafeqa')}}</button>
			                            @else
			                            	<button type="button" class="btn btn-blue" onclick="ajaxQa();">{{trans('home.valisafeqa')}}</button>
			                            @endif
			                        </form>
			                    </div>
                            </div>

                            <div id="safeemail" class="tab-pane">
                                <div id="form">
                                @if(empty($email) or $email_verify == 0)
				                    <form role="form" id="cellform" method="post" action="{{action('HomeController@postSafeemail')}}">
			                            <div class="row">
			                                <div class="col-sm-6">
			                                    <div class="form-group">
			                                        <label>{{trans('home.safeemail')}}</label>
			                                        <span class="input-icon icon-right">
			                                            <input type="text" name="email" value="{{$email}}" class="form-control">
			                                            <i class="glyphicon glyphicon-envelope palegreen"></i>
			                                        </span>
			                                    </div>
			                                </div>
			                                @if(!empty($email) and $email_verify == 0)
				                                <div class="col-sm-6">
				                                    <div class="form-group">
				                                        <p>&nbsp;</p>
				                                        <p><label><i class="fa fa-exclamation palered"></i> {{trans('home.waitverify')}}</label></p>
				                                    </div>
				                                </div>
			                                @endif
			                            </div>
			                            @if(empty($userinfo['safeq1']))
			                            	<button type="submit" class="btn btn-blue">{{trans('home.setsafemail')}}</button>
			                            @else
			                            	<button type="submit" class="btn btn-blue">{{trans('home.resafemail')}}</button>
			                            @endif
			                        </form>
			                    @else
			                    	<div class="row">
		                                <div class="col-sm-2">
		                                    <div class="form-group">
		                                        <label>{{trans('home.safeemail')}}</label>
		                                    </div>
		                                </div>
		                                <div class="col-sm-4">
		                                    <div class="form-group">
		                                        <label>{{substr(explode('@',$email)[0],0,2).'***@'.explode('@',$email)[1]}}</label>
		                                    </div>
		                                </div>
		                                <div class="col-sm-6">
		                                    <div class="form-group">
		                                        <label><i class="glyphicon glyphicon-ok palegreen"></i> {{trans('home.seted')}}</label>
		                                    </div>
		                                </div>
			                        </div>
			                    @endif
			                    </div>
                            </div>

                            <div id="safecell" class="tab-pane">
                               	<div id="form">
                                @if(empty($cell))
				                    <form role="form" id="cellform" method="post" action="{{action('HomeController@postSafecell')}}">
			                            <div class="row">
			                                <div class="col-sm-6">
			                                    <div class="form-group">
			                                        <label>{{trans('home.safecell')}}</label>
			                                        <span class="input-icon icon-right">
			                                            <input type="text" name="cell" class="form-control">
			                                            <i class="glyphicon glyphicon-phone palegreen"></i>
			                                        </span>
			                                    </div>
			                                </div>
			                            </div>
			                            <div class="row">
			                                <div class="col-sm-6">
			                                    <div class="form-group">
			                                        <label>{{trans('home.smscode')}}</label>
			                                        <div class="input-group">
                                                        <input type="text" name="smscode" class="form-control">
                                                        <span class="input-group-btn">
                                                            <button class="btn btn-default shiny" type="button" onclick="sendsms()">{{trans('home.getsmscode')}}</button>
                                                        </span>
                                                    </div>
			                                    </div>
			                                </div>
			                            </div>
			                            <button type="submit" class="btn btn-blue">{{trans('home.setsafecell')}}</button>
			                        </form>
			                    @else
			                    	<div class="row">
		                                <div class="col-sm-2">
		                                    <div class="form-group">
		                                        <label>{{trans('home.safecell')}}</label>
		                                    </div>
		                                </div>
		                                <div class="col-sm-4">
		                                    <div class="form-group">
		                                        <label>{{substr($cell,0,3).'****'.substr($cell,7,4)}}</label>
		                                    </div>
		                                </div>
		                                <div class="col-sm-6">
		                                    <div class="form-group">
		                                        <label><i class="glyphicon glyphicon-ok palegreen"></i> {{trans('home.seted')}}</label>
		                                    </div>
		                                </div>
			                        </div>
			                    @endif
			                    </div>
                            </div>

                            <div id="safeidentity" class="tab-pane">
                                <div id="form">
                                @if(empty($userinfo['realname']))
				                    <form role="form" id="base" method="post" action="{{action('HomeController@postSafeident')}}">
			                            <div class="row">
			                                <div class="col-sm-6">
			                                    <div class="form-group">
			                                        <label>{{trans('home.realname')}}</label>
			                                        <span class="input-icon icon-right">
			                                            <input type="text" name="realname" class="form-control">
			                                            <i class="glyphicon glyphicon-user palegreen"></i>
			                                        </span>
			                                    </div>
			                                </div>
			                                <div class="col-sm-6">
			                                    <div class="form-group">
			                                        <label>{{trans('home.idcard')}}</label>
			                                        <span class="input-icon icon-right">
			                                            <input type="text" name="idcard" class="form-control">
			                                            <i class="glyphicon glyphicon-barcode palegreen"></i>
			                                        </span>
			                                    </div>
			                                </div>
			                            </div>
			                            <p>*身份信息一经设置,仅支持二代身份证,无法修改,将作为您帐号所有权的依据,您可以凭填写信息对应的身份证找回帐号</p>
			                            <button type="submit" class="btn btn-blue">{{trans('home.setident')}}</button>
			                        </form>
			                    @else
			                    	<div class="row">
		                                <div class="col-sm-2">
		                                    <div class="form-group">
		                                        <label>{{trans('home.realname')}}</label>
		                                    </div>
		                                </div>
		                                <div class="col-sm-4">
		                                    <div class="form-group">
		                                        <label>{{'*'.mb_substr($userinfo['realname'],1,4)}}</label>
		                                    </div>
		                                </div>
		                                <div class="col-sm-6">
		                                    <div class="form-group">
		                                        <label><i class="glyphicon glyphicon-ok palegreen"></i> {{trans('home.seted')}}</label>
		                                    </div>
		                                </div>
			                        </div>
			                        <p>*身份信息一经设置,仅支持二代身份证,无法修改,将作为您帐号所有权的依据,您可以凭填写信息对应的身份证找回帐号</p>
			                    @endif
			                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>
</div>
<script>
	function ajaxQa(){
		$.ajax({
			cache: false,
			type: "POST",
			url: "{{action('HomeController@postSafeqa')}}",
			data: $('#safeqaform').serialize(),
			async: false,
			error: function(request) {
			    Notify("{{trans('home.connectionerror')}}", 'top-right', '5000', 'danger', 'fa-bolt', true);
			},
			success: function(data) {
				if(data['status'] == '1'){
					Notify("{{trans('home.safevaliok')}}", 'top-right', '5000', 'success', 'fa-check', true); 
				}else{
					Notify("{{trans('home.safevalino')}}", 'top-right', '5000', 'danger', 'fa-exclamation', true);
				}
				$('#safea1').val('');
				$('#safea2').val('');
				$('#safea3').val('');
			}
        });
	}
	function sendsms(){
		$.ajax({
			cache: false,
			type: "POST",
			url: "{{action('HomeController@postSendsms')}}",
			data: $('#cellform').serialize(),
			async: false,
			error: function(request) {
			    Notify("{{trans('home.connectionerror')}}", 'top-right', '5000', 'danger', 'fa-bolt', true);
			},
			success: function(data) {
				if(data['status'] == '1'){
					Notify(data['msg'], 'top-right', '5000', 'success', 'fa-check', true); 
				}else{
					Notify(data['msg'], 'top-right', '5000', 'danger', 'fa-exclamation', true);
				}
			}
        });
	}
</script>