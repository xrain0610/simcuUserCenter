<div class="page-body">
	<div class="row">
		<div class="col-lg-12 col-sm-12 col-xs-12">
            <div class="widget">
                <div class="widget-header bg-blue">
	                <span class="widget-caption">{{trans('home.editprofile')}}</span>
	            </div>
                <div class="widget-body">
                <div class="tabbable">
                        <ul class="nav nav-tabs tabs-flat" id="myTab11">
                            <li class="active">
                                <a data-toggle="tab" href="#baseprofile">
                                    {{trans('home.baseprofile')}}
                                </a>
                            </li>
                            <li class="">
                                <a data-toggle="tab" href="#extprofile">
                                    {{trans('home.extprofile')}}
                                </a>
                            </li>
                            <li class="">
                                <a data-toggle="tab" href="#changephoto">
                                    {{trans('home.changephoto')}}
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content tabs-flat">
                            <div id="baseprofile" class="tab-pane active">
                                <div id="form">
				                    <form role="form" id="base">
			                            <div class="row">
			                                <div class="col-sm-6">
			                                    <div class="form-group">
			                                        <label for="exampleInputEmail2">{{trans('home.showname')}}</label>
			                                        <span class="input-icon icon-right">
			                                            <input type="text" name="name" class="form-control" value="{{$name}}">
			                                            <i class="glyphicon glyphicon-user palegreen"></i>
			                                        </span>
			                                    </div>
			                                </div>
			                                <div class="col-sm-6">
			                                    <div class="form-group">
			                                        <label for="exampleInputName2">{{trans('home.sex')}}</label>
			                                        <div class="control-group">
			                                                <div class="radio">
			                                                    <label>
			                                                        <input name="sex" type="radio" value="GENDER" class="colored-blue" @if($sex == 'GENDER') checked="checked" @endif>
			                                                        <span class="text"> {{trans('home.gender')}}</span>
			                                                    </label>
			                                                    <label>
			                                                        &nbsp;<input name="sex" type="radio" value="FEMALE" class="colored-danger" @if($sex == 'FEMALE') checked="checked" @endif>
			                                                        <span class="text"> {{trans('home.female')}}</span>
			                                                    </label>
			                                                </div>
			                                            </div>
			                                    </div>
			                                </div>
			                            </div>
			                            <div class="row">
			                                <div class="col-sm-12">
			                                    <div class="form-group">
			                                        <label for="exampleInputEmail2">{{trans('home.signture')}}</label>
			                                        <span class="input-icon icon-right">
			                                            <textarea name="signture" class="form-control" rows="5">{{$signture}}</textarea>
			                                            <i class="glyphicon glyphicon-pencil"></i>
			                                        </span>
			                                   </div>
			                                </div>
			                            </div>
			                            <button type="button" class="btn btn-blue" onclick="ajaxForm('#base');">{{trans('home.updateprofile')}}</button>
			                        </form>
			                    </div>
                            </div>

                            <div id="extprofile" class="tab-pane">
                                <div id="form">
				                    <form role="form" id="ext">
			                            <div class="row">
			                                <div class="col-sm-6">
			                                    <div class="form-group">
			                                        <label for="exampleInputEmail2">{{trans('home.email')}}</label>
			                                        <span class="input-icon icon-right">
			                                            <input type="text" name="email" class="form-control" value="{{$email}}">
			                                            <i class="glyphicon glyphicon-envelope palegreen"></i>
			                                        </span>
			                                    </div>
			                                </div>
			                              	<div class="col-sm-6">
			                                    <div class="form-group">
			                                        <label for="exampleInputEmail2">{{trans('home.birthday')}}</label>
			                                        <div class="input-group">
	                                                <input class="form-control date-picker" name="birthday" id="id-date-picker-1" type="text" value="{{$birthday}}" data-date-format="dd-mm-yyyy">
	                                                <span class="input-group-addon">
	                                                    <i class="fa fa-calendar"></i>
	                                                </span>
                                            </div>
			                                    </div>
			                                </div>
			                            </div>
			                           	<div class="row">
			                                <div class="col-sm-6">
			                                    <div class="form-group">
			                                        <label for="exampleInputEmail2">{{trans('home.school')}}</label>
			                                        <span class="input-icon icon-right">
			                                            <input type="text" name="school" class="form-control" value="{{$school}}">
			                                            <i class="glyphicon glyphicon-book palegreen"></i>
			                                        </span>
			                                    </div>
			                                </div>
			                                <div class="col-sm-6">
			                                    <div class="form-group">
			                                        <label for="exampleInputEmail2">{{trans('home.language')}}</label>
			                                        <span class="input-icon icon-right">
			                                            <input type="text" name="language" class="form-control" value="{{$language}}">
			                                            <i class="glyphicon glyphicon-font palegreen"></i>
			                                        </span>
			                                    </div>
			                                </div>
			                            </div>
			                            <div class="row">
			                                <div class="col-sm-12">
			                                    <div class="form-group">
			                                        <label for="exampleInputEmail2">{{trans('home.personal_web')}}</label>
			                                        <span class="input-icon icon-right">
			                                            <input type="text" name="personal_web" class="form-control" value="{{$personal_web}}">
			                                            <i class="glyphicon glyphicon-home palegreen"></i>
			                                        </span>
			                                    </div>
			                                </div>
			                            </div>
			                            <div class="row">
			                                <div class="col-sm-6">
			                                    <div class="form-group">
			                                        <label for="exampleInputEmail2">{{trans('home.country')}}</label>
			                                        <span class="input-icon icon-right">
			                                            <input type="text" name="country" class="form-control" value="{{$country}}">
			                                            <i class="glyphicon glyphicon-globe palegreen"></i>
			                                        </span>
			                                    </div>
			                                </div>
			                                <div class="col-sm-3">
			                                    <div class="form-group">
			                                        <label for="exampleInputEmail2">{{trans('home.province')}}</label>
			                                        <span class="input-icon icon-right">
			                                            <input type="text" name="province" class="form-control" value="{{$province}}">
			                                            <i class="glyphicon glyphicon-tree-conifer palegreen"></i>
			                                        </span>
			                                    </div>
			                                </div>
			                                <div class="col-sm-3">
			                                    <div class="form-group">
			                                        <label for="exampleInputEmail2">{{trans('home.city')}}</label>
			                                        <span class="input-icon icon-right">
			                                            <input type="text" name="city" class="form-control" value="{{$city}}">
			                                            <i class="glyphicon glyphicon-tree-deciduous palegreen"></i>
			                                        </span>
			                                    </div>
			                                </div>
			                            </div>
			                            <div class="row">
			                                <div class="col-sm-12">
			                                    <div class="form-group">
			                                        <label for="exampleInputEmail2">{{trans('home.address')}}</label>
			                                        <span class="input-icon icon-right">
			                                            <textarea name="address" class="form-control" rows="5">{{$address}}</textarea>
			                                            <i class="glyphicon glyphicon-pencil"></i>
			                                        </span>
			                                   </div>
			                                </div>
			                            </div>
			                            <div class="row">
			                                <div class="col-sm-6">
			                                    <div class="form-group">
			                                        <label for="exampleInputEmail2">{{trans('home.company')}}</label>
			                                        <span class="input-icon icon-right">
			                                            <input type="text" name="company" class="form-control" value="{{$company}}">
			                                            <i class="glyphicon glyphicon-lock palegreen"></i>
			                                        </span>
			                                    </div>
			                                </div>
			                                <div class="col-sm-6">
			                                    <div class="form-group">
			                                        <label for="exampleInputEmail2">{{trans('home.vocation')}}</label>
			                                        <span class="input-icon icon-right">
			                                            <input type="text" name="vocation" class="form-control" value="{{$vocation}}">
			                                            <i class="glyphicon glyphicon-user palegreen"></i>
			                                        </span>
			                                    </div>
			                                </div>
			                            </div>
			                            <button type="button" class="btn btn-blue" onclick="ajaxForm('#ext');">{{trans('home.updateprofile')}}</button>
			                        </form>
			                    </div>
                            </div>

                            <div id="changephoto" class="tab-pane">
                                <div id="form">
				                    <form role="form" id="photo" enctype="multipart/form-data" method="post" action="{{action('HomeController@postProfile')}}">
			                           	<div class="row">
			                                <div class="col-sm-6">
			                                    <div class="form-group">
			                                        <img src = "{{$photo}}" style="width:100%">
			                                    </div>
			                                </div>
			                                <div class="col-sm-6">
			                                    <div class="form-group">
			                                        <label for="exampleInputEmail2">{{trans('home.selectphoto')}}</label>
			                                    </div>
			                                    <div class="form-group">
			                                        <span class="input-icon icon-right">
			                                            <input type="file" name="photo" class="form-control" value="{{$vocation}}" onchange="preview(this,'pphoto');">
			                                            <i class="glyphicon glyphicon-picture palegreen"></i>
			                                        </span>
			                                    </div>
			                                    <div class="form-group">
			                                        <img src = "" name="pphoto" style="width:70%" id="pphoto">
			                                    </div>
			                                    <div class="form-group">
			                                    	<button type="submit" class="btn btn-blue" onclick="ajaxForm('#photo');">{{trans('home.updateprofile')}}</button>
			                                	</div>
			                                </div>
			                            </div>
			                        </form>
			                    </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>
</div>
<script>
	function ajaxForm(id){
		$.ajax({
			cache: false,
			type: "POST",
			url: "{{action('HomeController@postProfile')}}",
			data: $(id).serialize(),
			async: false,
			error: function(request) {
			    Notify("{{trans('home.connectionerror')}}", 'top-right', '5000', 'danger', 'fa-bolt', true);
			},
			success: function(data) {
			    Notify("{{trans('home.updateprofilesuccess')}}", 'top-right', '5000', 'success', 'fa-check', true); 
			}
        });
	}
	function preview(eid,img){
    	document.getElementById(img).src =window.URL.createObjectURL(eid.files.item(0));
    }
</script>