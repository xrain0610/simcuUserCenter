<div class="lock-container animated fadeInDown">
        <div class="lock-box text-align-center">
            <div class="lock-username">DIVYIA PHILLIPS</div>
            <img src="assets/img/avatars/divyia.jpg" alt="divyia avatar" />
            <div class="lock-password">
                <form role="form" class="form-inline" action="index.html">
                    <div class="form-group">
                        <span class="input-icon icon-right">
                            <input class="form-control" placeholder="Password" type="password">
                            <i class="glyphicon glyphicon-log-in themeprimary"></i>
                        </span>
                    </div>
                </form>
            </div>

        </div>
        <div class="signinbox">
            <span>{{trans('passport.differentusers')}}</span>
            <a href="login.html">{{trans('passport.signinnow')}}</a>
        </div>
    </div>