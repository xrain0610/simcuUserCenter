<div class="login-container animated fadeInDown">
        <div class="loginbox bg-white">
            <br />
            <div class="loginbox-signup">
            	{{$notice}}
            </div>
            <div class="loginbox-signup">
                <a href="{{$backto}}">{{trans('passport.willbackto')}} {{trans('passport.backtorightnow')}}</a>
            </div>
            <br /><br />
        </div>
    </div>
    <meta http-equiv=refresh content=3;URL="{{$backto}}">
    <!--Basic Scripts-->
    <script src="/assets/js/jquery-2.0.3.min.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/slimscroll/jquery.slimscroll.min.js"></script>

    <!--Beyond Scripts-->
    <script src="/assets/js/beyond.js"></script>