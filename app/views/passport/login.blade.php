<div class="login-container animated fadeInDown">
        <div class="loginbox bg-white">
            <div class="loginbox-title">{{trans('passport.plogin')}}</div>
            <div class="loginbox-or">
                <div class="or-line"></div>
                <div class="or">Login</div>
            </div>
            <form action="{{action('PassportController@postLogin')}}" method="post">
            <div class="loginbox-textbox">
                <input type="text" name="username" class="form-control" placeholder="{{trans('passport.loginnamenotice')}}" />
            </div>
            <div class="loginbox-textbox">
                <input type="password" name="password" class="form-control" placeholder="{{trans('passport.password')}}" />
            </div>
            <div class="loginbox-textbox no-padding-bottom">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" class="colored-primary" name="keeplogin">
                        <span class="text darkgray">{{trans('passport.keeplogin')}}</span>
                    </label>
                </div>
            </div>
            <div class="loginbox-submit">
                <input type="submit" class="btn btn-primary btn-block" value="{{trans('passport.login')}}">
            </div>
            </form>
            <div class="loginbox-signup">
                <a href="{{action('PassportController@getRegister')}}">{{trans('passport.noaccount')}} {{trans('passport.register')}}</a>
            </div>
            <div class="loginbox-signup">
                <a href="{{action('PassportController@getRegister')}}">{{trans('passport.forgotpassword')}}</a>
            </div>
            <br />
        </div>
    </div>

    <!--Basic Scripts-->
    <script src="/assets/js/jquery-2.0.3.min.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/slimscroll/jquery.slimscroll.min.js"></script>

    <!--Beyond Scripts-->
    <script src="/assets/js/beyond.js"></script>