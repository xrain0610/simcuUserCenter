<div class="register-container animated fadeInDown">
        <div class="registerbox bg-white">
            <div class="registerbox-title">{{trans('passport.register')}}</div>

            <div class="registerbox-caption ">{{trans('passport.pleaseinput')}}</div>
            <form action="{{action('PassportController@postRegister')}}" method="post">
            <div class="registerbox-textbox">
                <input type="text" name="username" class="form-control" placeholder="{{trans('passport.username')}}" />
            </div>
            <div class="registerbox-textbox">
                <input type="password" name="password" class="form-control" placeholder="{{trans('passport.password')}}" />
            </div>
            <div class="registerbox-textbox">
                <input type="password" name="confpass" class="form-control" placeholder="{{trans('passport.confpass')}}" />
            </div>
            <div class="registerbox-textbox no-padding-bottom">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="agreed" class="colored-primary" checked="checked">
                        <span class="text darkgray">{{trans('passport.agreement')}}</span>
                    </label>
                </div>
            </div>
            <div class="registerbox-submit">
                <a href="{{action('PassportController@getLogin')}}">{{trans('passport.alreadyexist')}}</a><input type="submit" class="btn btn-primary pull-right" value="{{trans('passport.register')}}">
            </div>
            </form>
        </div>
    </div>

    <!--Basic Scripts-->
    <script src="/assets/js/jquery-2.0.3.min.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/slimscroll/jquery.slimscroll.min.js"></script>

    <!--Beyond Scripts-->
    <script src="/assets/js/beyond.min.js"></script>
    <!--Google Analytics::Demo Only-->
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'http://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-52103994-1', 'auto');
        ga('send', 'pageview');

    </script>