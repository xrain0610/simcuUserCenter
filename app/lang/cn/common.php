<?php
return [
	'backtoindex' => '返回首页',
	'pagenotfound' => '该页无法显示',
	'404notice' => '你访问的页面不存在',
	'500notice' => '服务器去火星开小差了,请稍后再试!',
];